package com.kodepagi.nursulaemansubmission3.util

// modifier
object Const {

    const val INTENT_DETAIL_MATCH = "INTENT_DETAIL_MATCH"
    const val INTENT_EVENT_ID = "INTENT_EVENT_ID"

}