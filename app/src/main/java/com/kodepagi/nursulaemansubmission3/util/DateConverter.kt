package com.kodepagi.nursulaemansubmission3.util

import java.text.SimpleDateFormat
import java.util.*

class DateConverter {

    companion object {
        const val DATE_24_HOURS = "dd-MM-yyyy HH:mm:ss"

        fun formatGMT(date: String?, time: String?): Date? {
            val formatter = SimpleDateFormat(DATE_24_HOURS, Locale.ENGLISH)
            formatter.timeZone = TimeZone.getTimeZone("UTC")
            val dateTime = "$date $time"
            return formatter.parse(dateTime)
        }

    }
}