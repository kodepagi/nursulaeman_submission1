package com.kodepagi.nursulaemansubmission3.model

data class TeamResponse(val teams: List<Team>)