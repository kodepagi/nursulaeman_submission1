package com.kodepagi.nursulaemansubmission3.model

data class TeamFootBall(val name: String?, val image: Int?, val detail: String?)