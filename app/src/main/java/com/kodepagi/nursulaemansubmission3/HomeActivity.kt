package com.kodepagi.nursulaemansubmission3

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.kodepagi.nursulaemansubmission3.R.id.*
import com.kodepagi.nursulaemansubmission3.R.layout.activity_home
import com.kodepagi.nursulaemansubmission3.ui.match.FavMatchFragment
import com.kodepagi.nursulaemansubmission3.ui.match.LastMatchFragment
import com.kodepagi.nursulaemansubmission3.ui.match.NextMatchFragment
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(activity_home)

        bottom_navigation.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                last -> {
                    setupListLastMacthFragment(savedInstanceState)
                }
                next -> {
                    setupListNextMacthFragment(savedInstanceState)
                }
                fav -> {
                    setupListFavMacthFragment(savedInstanceState)
                }
            }
            true
        }
        bottom_navigation.selectedItemId = last

    }

    private fun setupListLastMacthFragment(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.main_container, LastMatchFragment(), LastMatchFragment::class.simpleName)
                    .commit()
        }
    }

    private fun setupListNextMacthFragment(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.main_container, NextMatchFragment(), NextMatchFragment::class.simpleName)
                    .commit()
        }
    }

    private fun setupListFavMacthFragment(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.main_container, FavMatchFragment(), FavMatchFragment::class.simpleName)
                    .commit()
        }
    }

}
