package com.kodepagi.nursulaemansubmission3.ui.match

import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kodepagi.nursulaemansubmission3.R
import com.kodepagi.nursulaemansubmission3.model.Match
import kotlinx.android.synthetic.main.match_adapter.view.*

class MatchAdapter(private val context: FragmentActivity?,
                   private val matchs: List<Match>,
                   private val detailMatch: (Match) -> Unit)
    : RecyclerView.Adapter<MatchViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MatchViewHolder {
        return MatchViewHolder(LayoutInflater.from(context).inflate(R.layout.match_adapter, parent, false))
    }

    override fun getItemCount(): Int = matchs.size

    override fun onBindViewHolder(holder: MatchViewHolder, position: Int) {
        holder.bindItem(matchs[position], detailMatch)
    }
}

class MatchViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun bindItem(matchs: Match, detailMatch: (Match) -> Unit) {
        itemView.date_event.text = matchs.dateEvent
        itemView.tv_home_team.text = matchs.strHomeTeam
        itemView.tv_away_team.text = matchs.strAwayTeam
        itemView.tv_score_home.text = matchs.intHomeScore
        itemView.tv_score_away.text = matchs.intAwayScore


        itemView.setOnClickListener { detailMatch(matchs) }
    }
}