package com.kodepagi.nursulaemansubmission3.ui.match

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kodepagi.nursulaemansubmission3.R.layout.fragment_favmatch
import com.kodepagi.nursulaemansubmission3.db.Favorite
import com.kodepagi.nursulaemansubmission3.db.database
import com.kodepagi.nursulaemansubmission3.model.Match
import com.kodepagi.nursulaemansubmission3.util.Const
import kotlinx.android.synthetic.main.fragment_nextmatch.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select

class FavMatchFragment : Fragment() {

    private var matchs: MutableList<Match> = mutableListOf()
    private lateinit var adapter: MatchAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(fragment_favmatch, container, false)

        return view
    }

    override fun onResume() {
        super.onResume()

        showFavorite()
    }


    private fun showFavorite() {
        context?.database?.use {
            val result = select(Favorite.TABLE_FAVORITE)
            val event = result.parseList(classParser<Match>())
            matchs.clear()
            matchs.addAll(event)

            rv_next_match.layoutManager = LinearLayoutManager(activity)
            adapter = MatchAdapter(activity, matchs) {
                val intent = Intent(context, MatchDetailActivity::class.java)
                intent.putExtra(Const.INTENT_EVENT_ID, it.idEvent)
                startActivity(intent)
            }
            rv_next_match.adapter = adapter
            adapter.notifyDataSetChanged()
        }
    }


}

