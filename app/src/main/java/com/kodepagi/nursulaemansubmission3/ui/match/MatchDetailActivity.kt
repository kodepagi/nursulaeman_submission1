package com.kodepagi.nursulaemansubmission3.ui.match

import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import com.google.gson.Gson
import com.kodepagi.nursulaemansubmission3.R
import com.kodepagi.nursulaemansubmission3.R.drawable.ic_add_to_favorite
import com.kodepagi.nursulaemansubmission3.R.drawable.ic_added_to_favorite
import com.kodepagi.nursulaemansubmission3.R.id.add_to_favorite
import com.kodepagi.nursulaemansubmission3.R.menu.detail_match_menu
import com.kodepagi.nursulaemansubmission3.api.ApiRepository
import com.kodepagi.nursulaemansubmission3.db.Favorite
import com.kodepagi.nursulaemansubmission3.db.database
import com.kodepagi.nursulaemansubmission3.model.Match
import com.kodepagi.nursulaemansubmission3.model.Team
import com.kodepagi.nursulaemansubmission3.util.Const
import com.kodepagi.nursulaemansubmission3.util.DateConverter
import com.kodepagi.nursulaemansubmission3.vp.team.TeamPresenter
import com.kodepagi.nursulaemansubmission3.vp.team.TeamView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_match_detail.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.jetbrains.anko.design.snackbar
import java.text.SimpleDateFormat

class MatchDetailActivity : AppCompatActivity(), TeamView {

    private lateinit var match: Match
    private lateinit var teamPresenter: TeamPresenter
    private var idEvent: String = ""
    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false
    private lateinit var progressBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_match_detail)

        idEvent = intent.getStringExtra(Const.INTENT_EVENT_ID)

        progressBar = findViewById(R.id.pb_away_image)

        val request = ApiRepository()
        val gson = Gson()
        teamPresenter = TeamPresenter(this, request, gson)

        teamPresenter.getEvent(idEvent)

    }

    override fun showLoading() {
        pb_home_image.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        pb_home_image.visibility = View.GONE
    }

    override fun showEvent(data: List<Match>) {

        match = data.get(0)

        tv_date.text = match.dateEvent
        tv_detail_home_team.text = match.strHomeTeam
        tv_detail_away_team.text = match.strAwayTeam
        tv_detail_score_home.text = match.intHomeScore
        tv_detail_score_away.text = match.intAwayScore

        tv_goals_home.text = match.strHomeGoalDetails
        tv_goals_away.text = match.strAwayGoalDetails
        tv_shoot_home.text = match.intHomeShots
        tv_shoot_away.text = match.intAwayShots

        tv_home_goals_keeper.text = match.strHomeLineupGoalkeeper
        tv_away_goals_keeper.text = match.strAwayLineupGoalkeeper
        tv_home_defend.text = match.strHomeLineupDefense
        tv_away_defend.text = match.strAwayLineupDefense
        tv_home_midfield.text = match.strHomeLineupMidfield
        tv_away_midfield.text = match.strAwayLineupMidfield
        tv_home_forward.text = match.strHomeLineupForward
        tv_away_forward.text = match.strAwayLineupForward
        tv_home_subs.text = match.strHomeLineupSubstitutes
        tv_away_subs.text = match.strAwayLineupSubstitutes

        try {
            val dateTime = DateConverter.formatGMT(match.dateEvent, match.strTime)
            val sdf = SimpleDateFormat("HH:mm").format(dateTime)
            tv_time.text = sdf
        } catch (e: Exception) {
            e.printStackTrace()
        }

        teamPresenter.getTeamHome(match.idHomeTeam)
        teamPresenter.getTeamAway(match.idAwayTeam)

        favoriteState()
    }

    override fun showTeamHome(data: List<Team>) {
        pb_home_image.visibility = View.GONE
        iv_detail_home_team.visibility = View.VISIBLE

        Picasso.get().load(data[0].strTeamBadge).into(iv_detail_home_team)
    }

    override fun showTeamAway(data: List<Team>) {
        pb_away_image.visibility = View.GONE
        iv_detail_away_team.visibility = View.VISIBLE

        Picasso.get().load(data[0].strTeamBadge).into(iv_detail_away_team)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(detail_match_menu, menu)
        menuItem = menu
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            add_to_favorite -> {
                if (isFavorite)
                    removeFromFavorite() else addToFavorite()

                isFavorite = !isFavorite
                setFavorite()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun addToFavorite() {
        try {
            database.use {
                insert(Favorite.TABLE_FAVORITE,
                        Favorite.EVENT_ID to match.idEvent,
                        Favorite.EVENT_DATE to match.dateEvent,
                        Favorite.EVENT_HOME_NAME to match.strHomeTeam,
                        Favorite.EVENT_AWAY_NAME to match.strAwayTeam,
                        Favorite.EVENT_HOME_SCORE to match.intHomeScore,
                        Favorite.EVENT_AWAY_SCORE to match.intAwayScore,
                        Favorite.EVENT_HOME_GOALS to match.strHomeGoalDetails,
                        Favorite.EVENT_AWAY_GOALS to match.strAwayGoalDetails,
                        Favorite.EVENT_HOME_SHOTS to match.intHomeShots,
                        Favorite.EVENT_AWAY_SHOTS to match.intAwayShots,
                        Favorite.EVENT_HOME_GOALS_KEEPER to match.strHomeLineupGoalkeeper,
                        Favorite.EVENT_AWAY_GOALS_KEEPER to match.strAwayLineupGoalkeeper,
                        Favorite.EVENT_HOME_DEFENS to match.strHomeLineupDefense,
                        Favorite.EVENT_AWAY_DEFENS to match.strAwayLineupDefense,
                        Favorite.EVENT_HOME_MIDFIELD to match.strHomeLineupMidfield,
                        Favorite.EVENT_AWAY_MIDFIELD to match.strAwayLineupMidfield,
                        Favorite.EVENT_HOME_GOALS_FORWARD to match.strHomeLineupForward,
                        Favorite.EVENT_AWAY_GOALS_FORWARD to match.strAwayLineupForward,
                        Favorite.EVENT_HOME_GOALS_SUBSTITUTES to match.strHomeLineupSubstitutes,
                        Favorite.EVENT_AWAY_GOALS_SUBSTITUTES to match.strAwayLineupSubstitutes,
                        Favorite.EVENT_HOME_TEAM_ID to match.idHomeTeam,
                        Favorite.EVENT_AWAY_TEAM_ID to match.idAwayTeam,
                        Favorite.EVENT_TIME to match.strTime,
                        Favorite.EVENT_SPORT to match.strSport)
            }
            progressBar.snackbar("Added to favorite").show()
        } catch (e: SQLiteConstraintException) {
            progressBar.snackbar(e.localizedMessage).show()
        }
    }

    private fun removeFromFavorite() {
        try {
            database.use {
                delete(Favorite.TABLE_FAVORITE,
                        "(" + Favorite.EVENT_ID + "={event_id})",
                        "event_id" to idEvent
                )
            }
            progressBar.snackbar("Removed to favorite").show()
        } catch (e: SQLiteConstraintException) {
            progressBar.snackbar(e.localizedMessage).show()
        }
    }

    private fun setFavorite() {
        if (isFavorite)
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, ic_added_to_favorite)
        else
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, ic_add_to_favorite)
    }

    private fun favoriteState() {
        database.use {
            val result = select(Favorite.TABLE_FAVORITE)
                    .whereArgs("(EVENT_ID = {id})",
                            "id" to idEvent)
            val favorite = result.parseList(classParser<Match>())
            if (!favorite.isEmpty()) isFavorite = true

            setFavorite()
        }

    }

}