package com.kodepagi.nursulaemansubmission3.ui.match

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.kodepagi.nursulaemansubmission3.R
import com.kodepagi.nursulaemansubmission3.R.layout.fragment_nextmatch
import com.kodepagi.nursulaemansubmission3.api.ApiRepository
import com.kodepagi.nursulaemansubmission3.model.Match
import com.kodepagi.nursulaemansubmission3.util.Const
import com.kodepagi.nursulaemansubmission3.vp.match.MatchPresenter
import com.kodepagi.nursulaemansubmission3.vp.match.MatchView
import kotlinx.android.synthetic.main.fragment_nextmatch.*


class NextMatchFragment : Fragment(), MatchView {

    private var matchs: MutableList<Match> = mutableListOf()
    private lateinit var adapter: MatchAdapter
    private lateinit var matchPresenter: MatchPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(fragment_nextmatch, container, false)

        return view
    }

    override fun onResume() {
        super.onResume()

        initData()
    }

    private fun initData() {
        val request = ApiRepository()
        val gson = Gson()
        matchPresenter = MatchPresenter(this, request, gson)

        matchPresenter.getNextMatch(resources.getString(R.string.league_id))

    }


    override fun showLoading() {
        progressBar?.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressBar?.visibility = View.GONE
    }

    override fun showMatchList(data: List<Match>) {
        matchs.clear()
        matchs.addAll(data)

        rv_next_match.layoutManager = LinearLayoutManager(activity)
        adapter = MatchAdapter(activity, matchs) {
            val intent = Intent(context, MatchDetailActivity::class.java)
            intent.putExtra(Const.INTENT_EVENT_ID, it.idEvent)
            startActivity(intent)
        }
        rv_next_match.adapter = adapter
    }

}

