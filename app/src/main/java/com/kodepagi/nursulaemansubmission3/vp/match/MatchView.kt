package com.kodepagi.nursulaemansubmission3.vp.match

import com.kodepagi.nursulaemansubmission3.model.Match

interface MatchView {

    fun showLoading()
    fun hideLoading()
    fun showMatchList(data: List<Match>)

}