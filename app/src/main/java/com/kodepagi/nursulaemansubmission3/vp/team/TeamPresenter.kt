package com.kodepagi.nursulaemansubmission3.vp.team

import com.google.gson.Gson
import com.kodepagi.nursulaemansubmission3.api.ApiRepository
import com.kodepagi.nursulaemansubmission3.api.TheSportDBApi
import com.kodepagi.nursulaemansubmission3.model.MatchResponse
import com.kodepagi.nursulaemansubmission3.model.TeamResponse
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class TeamPresenter(private val view: TeamView,
                    private val apiRepository: ApiRepository,
                    private val gson: Gson) {

    fun getEvent(eventId: String?) {
        view.showLoading()
        doAsync {
            val data = gson.fromJson(apiRepository
                    .doRequest(TheSportDBApi.getEvent(eventId)),
                    MatchResponse::class.java
            )

            uiThread {
                view.hideLoading()
                view.showEvent(data.events)
            }
        }
    }

    fun getTeamHome(teamId: String?) {
        view.showLoading()
        doAsync {
            val data = gson.fromJson(apiRepository
                    .doRequest(TheSportDBApi.getTeam(teamId)),
                    TeamResponse::class.java
            )

            uiThread {
                view.hideLoading()
                view.showTeamHome(data.teams)
            }
        }
    }

    fun getTeamAway(teamId: String?) {
        view.showLoading()
        doAsync {
            val data = gson.fromJson(apiRepository
                    .doRequest(TheSportDBApi.getTeam(teamId)),
                    TeamResponse::class.java
            )

            uiThread {
                view.hideLoading()
                view.showTeamAway(data.teams)
            }
        }
    }

}

