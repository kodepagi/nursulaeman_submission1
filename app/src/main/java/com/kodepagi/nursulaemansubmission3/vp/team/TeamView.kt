package com.kodepagi.nursulaemansubmission3.vp.team


import com.kodepagi.nursulaemansubmission3.model.Match
import com.kodepagi.nursulaemansubmission3.model.Team

interface TeamView {

    fun showLoading()
    fun hideLoading()
    fun showEvent(data: List<Match>)
    fun showTeamHome(data: List<Team>)
    fun showTeamAway(data: List<Team>)

}