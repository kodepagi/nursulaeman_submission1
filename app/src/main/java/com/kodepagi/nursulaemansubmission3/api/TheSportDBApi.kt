package com.kodepagi.nursulaemansubmission3.api

import com.kodepagi.nursulaemansubmission3.BuildConfig

object TheSportDBApi {

    fun getLastMatch(leagueId: String?): String {
        return "${BuildConfig.BASE_URL}${BuildConfig.TSDB_API_KEY}" +
                "/eventspastleague.php?id=${leagueId}"
    }

    fun getNextMatch(leagueId: String?): String {
        return "${BuildConfig.BASE_URL}${BuildConfig.TSDB_API_KEY}" +
                "/eventsnextleague.php?id=${leagueId}"
    }

    fun getEvent(eventId: String?): String {
        return "${BuildConfig.BASE_URL}${BuildConfig.TSDB_API_KEY}" +
                "/lookupevent.php?id=${eventId}"
    }

    fun getTeam(teamId: String?): String {
        return "${BuildConfig.BASE_URL}${BuildConfig.TSDB_API_KEY}" +
                "/lookupteam.php?id=${teamId}"
    }

}